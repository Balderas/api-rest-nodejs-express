const express = require('express');
const Transaction = require('../models/transaction_model');
const Account = require('../models/account_model');
const ruta = express.Router();

//FindByIdAccount, return balance
ruta.get('/:idAccount/:mode',(req, res) => {
    if(parseInt(req.params.mode) === 0){
        res.json(findTransacctionsReceivedByIdAccount(req.params.idAccount));
    } 
    if(parseInt(req.params.mode) === 1) {
        res.json(findTransacctionsSendByIdAccount(req.params.idAccount));
    }
});

//FindByIdAccount, return balance
ruta.get('/:idAccount',(req, res) => {
    res.json(findTransacctionsByIdAccount(req.params.idAccount));
});

// Get all Transactions
ruta.get('/',(req, res) => {
    res.json(Transaction.transactionList)
});

ruta.post('/', (req, res) => {
    let body = req.body;
    let fromAccount =  findByIdAccount(body.fromAccount);
    if(body.amount != null && body.amount > 0){
            let amount = fromAccount.amount - body.amount;
            if(amount > -500){
                fromAccount.amount = amount;
                removeItemFromArr(fromAccount.id);
                Account.accountList.push(fromAccount);
                
                let toAccount =  findByIdAccount(body.toAccount);
                amount = toAccount.amount + body.amount;
                toAccount.amount = amount;
                removeItemFromArr(toAccount.id);
                Account.accountList.push(toAccount);
            
                let newRegister =  {
                    id:Transaction.transactionList.length + 1, 
                    "fromAccount":fromAccount.id,
                    "toAccount" : toAccount.id,
                    "amount" :body.amount,
                    "createdAt" :new Date()
                    };
                Transaction.transactionList.push(newRegister);
                res.json(newRegister);
            }else{
                let newRegister =  {"message":"Saldo no puede ser menor a 500","status":404}
                res.status(404).json(newRegister);
                        res.status(404).json(newRegister);

            }
    }else{
        let newRegister =  {"message":"Monto tiene que ser numérico y mayor a 0","status":404}
        res.status(404).json(newRegister);
    }
}); 

function findByIdAccount(id){
    return Account.accountList.find(u => u.id === parseInt(id));
}

function findTransacctionsByIdAccount(id){
    let returnList =  [];
    Transaction.transactionList.forEach(function(transaction) {
        if (transaction.toAccount === parseInt(id)  || transaction.fromAccount === parseInt(id)  ) {
          returnList.push(transaction);
        }
      });
    return returnList;
}


function findTransacctionsReceivedByIdAccount(id){
    let returnList =  [];
    Transaction.transactionList.forEach(function(transaction) {
        if (transaction.toAccount === parseInt(id)  ) {
          returnList.push(transaction);
        }
      });
    return returnList;
}

function findTransacctionsSendByIdAccount(id){
    let returnList =  [];
    Transaction.transactionList.forEach(function(transaction) {
        if (transaction.fromAccount === parseInt(id)  ) {
          returnList.push(transaction);
        }
      });
    return returnList;
}

function removeItemFromArr ( id ) {
    const item = findByIdAccount (id);
    var i = Account.accountList.indexOf( item );
    Account.accountList.splice( i, 1 );
}

module.exports = ruta;