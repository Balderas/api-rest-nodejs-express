const express = require('express');
const Owner = require('../models/owner_model');
const ruta = express.Router();

ruta.get('/',(req, res) => {
    res.json(Owner.ownerList)
});

module.exports = ruta;