const express = require('express');
const Account = require('../models/account_model');
const ruta = express.Router();

ruta.get('/',(req, res) => {
    res.json(Account.accountList)
});

//FindByIdAccount, return balance
ruta.get('/:idAccount',(req, res) => {
    const reg = findByIdAccount(req.params.idAccount);
    var register = [
        {"account":reg.id, "owner":reg.ownerId, "balance" : reg.amount, "createdAt" : reg.createdAt}
    ]
    res.json(register);
});

function findByIdAccount(id){
    return Account.accountList.find(u => u.id === parseInt(id));
}
module.exports = ruta;