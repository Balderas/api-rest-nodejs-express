﻿const owners = require('./routes/owner');
const transactions = require('./routes/transaction');
const accounts = require('./routes/account');

const express = require('express');
const app = express(); // Instance

router = express.Router(),
bodyParser = require('body-parser'),
swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/v1', router);

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use('/api/v1/owner', owners);
app.use('/api/v1/transaction', transactions);
app.use('/api/v1/account', accounts);

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log('Api RESTFull Ok,and running...'+port);
})