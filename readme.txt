Requirements 
 
Node v12.16.1 npm 6.13.4 
 
Postman is recommended to access the endpoints https://www.getpostman.com/ 
 
Installation and configuration ID 
 
- Unzip the code and place it in a folder with permission 
 - To download ID Visual Studio Code, from the URL = ?https://code.visualstudio.com/ 
 - Run installer of  Visual Studio Code 
 
- Import the folder 
 
- Open an operating system console 
 
- Within the project folder, in the root run: node app.js 
 
Swagger requirements 
 
- Within the project folder, in the root run : npm i swagger-ui-express -S swagger url : ?http://localhost:3000/api-docs/ Note: For parameterization reasons it is recommended to create an environment variable to             define the port on which the api will be listening. 
- Inside the console with administrator privileges 
Windows environment: set PORT = 5000 
MAC , Linux: export PORT = 5000 
By default the project runs on port 3000 
 
 